# Adapted for use in Home Assistant by Dan C Williams, tlskinneriv

# https://austinsnerdythings.com/2021/03/20/handling-data-from-ambient-weather-ws-2902c-to-mqtt/?unapproved=64&moderation-hash=2c93b4c769f98120c9adae6be6ca2f18#comment-64
# Python script to decode Ambient Weather data (from WS-2902C and similar)
# and publish to MQTT.
# original author: Austin of austinsnerdythings.com
# publish date: 2021-03-20
# jsr modified to calculate and provide the dewpoint and feelslike temperatures to the awnet_local integration 2022-08-08

from urllib.parse import parse_qs, quote
import json
import requests
import os
import logging
from wsgiref.simple_server import make_server
from wsgiref.simple_server import WSGIRequestHandler
from wsgiref.headers import Headers
import sys
from datetime import datetime,timezone
import math
import configparser
from pathlib import Path

# set vars
# remember. I have to create a long term token and use that for the "SUPERVISOR_TOKEN"
AUTH_TOKEN = os.getenv("SUPERVISOR_TOKEN", "test")
#HA_SERVER = os.getenv("HA_HOST", "http://supervisor/core/api/services/awnet_local/update")
# the above is what the "ADD_ON" had for the url.

# this is how to "connect" to homeassistant
# the "localhost" for my bridged docker network is 172.17.0.1
# the port my docker homeassistant is listening on is 8123
# the awnet_local is the custom integration folder and the update method is the callback
HA_SERVER = os.getenv("HA_HOST", "http://172.17.0.1:8123/api/services/awnet_local/update")

# I also wanted to control the logging from the webserver. It was logging to stderr by default
LOGGING = os.getenv("HTTP_SERVER_LOG", "NONE")

_LOGGER = logging.getLogger(__name__)
_CONFIG = 'config/awnet.ini'

# Global variable for saving event rain
is_raining = False
last_rain_dt = None
rain_24hr = 0.0
rain_yearly = 0.0
server_port = 8080

# setup persistent data from ini file parser
config = configparser.ConfigParser()
# create file if it doesn't exist
file = Path(_CONFIG)
file.touch(exist_ok=True)
config.read(_CONFIG)

# from https://stackoverflow.com/questions/31433682/control-wsgiref-simple-server-log
class NoLoggingWSGIRequestHandler(WSGIRequestHandler):
    def log_message(self, format, *args):
        if LOGGING != "NONE":
            sys.stderr.write("%s - - [%s] %s\n" %
                     (self.client_address[0],
                      self.log_date_time_string(),
                      format%args))
        else:
            pass

def read_config(section, option):
    global config
    value = None
    if config.has_section(section):
        if config.has_option(section, option):
            value = config[section][option]
    return value

def write_config(section, option, value):
    global config
    if config.has_section(section):
        config[section][option] = value
    else:
        config[section] = {option: value}

def save_config():
    global config
    # save the config object back to file
    with open(_CONFIG,"w") as file_object:
        config.write(file_object)

def get_headers(environ):
    """
    Handles getting the headers from the environment, Content-Type is the special one
    """
    headers = Headers([])
    for header, value in environ.items():
        if header.startswith("HTTP_"):
            headers[header[5:].replace('_','-')] = value
    if 'CONTENT_TYPE' in environ:
        headers['CONTENT-TYPE'] = environ['CONTENT_TYPE']
    return headers

def publish(payload):
    payload_json = json.dumps(payload)

    head = {
        "Authorization": "Bearer " + AUTH_TOKEN,
        "content-type": "application/json",
    }
    good_responses = [200, 201]

    url = HA_SERVER

    try:
        response = requests.post(url, data=payload_json, headers=head)
        if response.status_code in good_responses:
            _LOGGER.info(f"Sent {payload_json}")
        else:
            _LOGGER.error(f"Failed to send {payload_json} to {url} with headers {head}; {response.content}")
    except requests.exceptions.Timeout:
        _LOGGER.error(f"Timeout connecting to {url}")
    except requests.exceptions.ConnectionError:
        _LOGGER.error(f"Couldn't connect to {url}")
    except:
        _LOGGER.error(f"Unknown error connecting to {url}")
        pass

def handle_results(result):
    """result is a dict. full list of variables include:
    stationtype: ['AMBWeatherV4.2.9'], PASSKEY: ['<station_mac_address>'], dateutc: ['2021-03-20 17:12:27'], tempinf: ['71.1'], humidityin: ['36'], baromrelin: ['29.693'],    baromabsin: ['24.549'],    tempf: ['58.8'], battout: ['1'], humidity: ['32'], winddir: ['215'],windspeedmph: ['0.0'],    windgustmph: ['0.0'], maxdailygust: ['3.4'], hourlyrainin: ['0.000'],    eventrainin: ['0.000'],    dailyrainin: ['0.000'],
    weeklyrainin: ['0.000'], monthlyrainin: ['0.000'], totalrainin: ['0.000'],    solarradiation: ['121.36'],
    uv: ['1'],batt_co2: ['1']"""
    # This changes the resulting key values from lists to single values
    for key in result:
        if len(result[key]) == 1:
            result[key] = result[key][0]
        else:
            _LOGGER.error('Unexpected list size for key %s', key)
    result = handle_calculated_results(result)
    publish(result)

def dewpoint(temp,humidity):
    # temp is in Farenheit
    # humidity is in %
    # Round to nearest degree. Close enough.
    tempC = (temp - 32) * 5.0 / 9.0
    MAGNUS_K2 = 17.62
    MAGNUS_K3 = 243.12
    alpha = MAGNUS_K2 * tempC / (MAGNUS_K3 + tempC)
    beta = MAGNUS_K2 * MAGNUS_K3 / (MAGNUS_K3 + tempC)
    dewPoint = round((MAGNUS_K3 * (alpha + math.log(humidity / 100.0)) / (beta - math.log(humidity / 100.0))*9.0/5.0+32.0), 0)
    _LOGGER.debug('Dewpoint Magnus: %s', str(dewPoint))
    # Ambient Weather uses Arden Buck Eq.
    B = 18.678
    C = 257.14
    D = 234.5
    GAMMA_T_RH = math.log((humidity/100) * math.pow(math.e, (B - (tempC / D)) * (tempC / (C + tempC))))
    dew_pt = round((C * GAMMA_T_RH / (B - GAMMA_T_RH) * 9.0/5.0 + 32), 0)
    # dewPoint is in Farenheit
    _LOGGER.debug('Dewpoint Arden Buck: %s', str(dew_pt))
    return min(dewPoint, dew_pt)

def feelslike(temp,humidity,windspeed=0.0):
    # temp is in Farenheit
    # humidity is in %
    # windspeed is in mph
    feelsLike = temp
    # zero wind doesn't work!
    # after playing around with a few different formulas, I decided upon about 55 degrees and 2 mph as a cutoff
    # for the NWS wind chill chart. I increased the lower bound from 50 to 55.
    if temp < 55.0 and windspeed > 2.0:
        # windchill
        feelsLike = 35.74 + (0.6215 * temp) - 35.75 * (windspeed**0.16) + (0.4275 * temp) * (windspeed**0.16)
        if feelsLike > temp:
            feelsLike = temp

    # I was using the NWS 1990 model, see: https://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml
    # After investigating 21!!! different Heat Index formulas, the recommendation is to use NWS 2011.
    # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3801457/

    # 2011 NWS flowchart uses 40. But, I use 55 in windchill, so start from there.
    # feelsLike is in Farenheit
    if temp >= 55.0:
        feelsLike = -10.3 + 1.1*temp + 0.047*humidity
        if feelsLike >= 79.0:
            feelsLike = (-42.379 + 2.04901523 * temp + 10.14333127 * humidity - 0.22475541 * temp * humidity 
                     - 0.00683783 * temp * temp - 0.05481717 * humidity * humidity + 0.00122874 * temp * temp * humidity
                     + 0.00085282 * temp * humidity * humidity - 0.00000199 * temp * temp * humidity * humidity)
            if humidity <= 13.0 and temp >= 80.0 and temp <= 112.0:
                feelsLike = feelsLike - ((13.0 - humidity) / 4.0) * math.sqrt((17.0 - math.fabs(temp - 95.0)) / 17.0)
            elif humidity > 85.0 and temp >= 80.0 and temp <= 87.0:
                feelsLike = feelsLike + ((humidity - 85.0) / 10.0) * ((87.0 - temp) / 5.0)
        if feelsLike < temp:
            feelsLike = temp

    feelsLike = round(feelsLike,0)
    _LOGGER.debug('FeelsLike: ', str(feelsLike))
    return feelsLike

def feelslike2(temp,humidity,windspeed=0.0):
    # temp is in Farenheit
    # humidity is in %
    # windspeed is in mph
    feelsLike = temp
    # this is the 16 parameter Stull equation
    feelsLike = (16.923 + 0.185212 * temp + 5.37941 * humidity - 0.100254 * temp * humidity 
        + 9.41695 * math.pow(10, -3) * math.pow(temp, 2) + 7.28898 * math.pow(10, -3) 
        * math.pow(humidity, 2) + 3.45372 * math.pow(10, -4) * math.pow(temp, 2) * humidity 
        - 8.14971 * math.pow(10, -4) * temp * math.pow(humidity, 2) + 1.02102 * math.pow(10, -5) 
        * math.pow(temp, 2) * math.pow(humidity, 2) - 3.8646 * math.pow(10, -5) * math.pow(temp, 3) 
        + 2.91583 * math.pow(10, -5) * math.pow(humidity, 3) + 1.42721 * math.pow(10, -6) 
        * math.pow(temp, 3) * humidity + 1.97483 * math.pow(10, -7) * temp * math.pow(humidity, 3) 
        - 2.18429 * math.pow(10, -8) * math.pow(temp, 3) * math.pow(humidity, 2) + 8.43296 
        * math.pow(10, -10) * math.pow(temp, 2) * math.pow(humidity, 3) - 4.81975 * math.pow(10, -11) 
        * math.pow(temp, 3) * math.pow(humidity, 3))
    if feelsLike < temp:
        feelsLike = temp
    feelsLike = round(feelsLike,0)
    _LOGGER.debug('FeelsLike: ', str(feelsLike))
    return feelsLike

def handle_calculated_results(result):
    """add missing data that needs to be calculated
    lastRain
    dewPoint
    dewPointin
    feelsLike
    feelsLikein
    """
    global is_raining
    global last_rain_dt
    global rain_24hr
    global rain_yearly
    temp = 0.0
    windspeed = 0.0
    humidity = 0.0
    if 'tempf' in result:
        temp = float(result['tempf'])
        windspeed = float(result['windspeedmph'])
        humidity = float(result['humidity'])
        dewPoint = dewpoint(temp,humidity)
        result['dewPoint'] = str(dewPoint)
        feelsLike = feelslike(temp,humidity,windspeed)
        result['feelsLike'] = str(feelsLike)
    if 'tempinf' in result:
        temp = float(result['tempinf'])
        windspeed = 0.0
        humidity = float(result['humidityin'])
        dewPoint = dewpoint(temp,humidity)
        result['dewPointin'] = str(dewPoint)
        feelsLike = feelslike(temp,humidity,windspeed)
        result['feelsLikein'] = str(feelsLike)
    # Additional passthru sensor TODO fix code for multiple other sensors
    if 'temp1f' in result:
        temp = float(result['temp1f'])
        humidity = float(result['humidity1'])
        # these values are not part of the awnet_local integration
        # TODO gracefully handle these somehow in the awnet_local integration
        # maybe a better thing would be to add my own custom dewpoint sensors for these instead
        # I do have a custom dewpoint integration that creates a dewpoint sensor for HACS
        dewPoint1 = dewpoint(temp,humidity)
        result['dewPoint1'] = str(dewPoint1)
        feelsLike1 = feelslike(temp,humidity)
        result['feelsLike1'] = str(feelsLike1)

    rain = 0.0
    if 'eventrainin' in result:
        rain = float(result['eventrainin'])

    rain_rate = 0.0
    if 'hourlyrainin' in result:
        rain_rate = float(result['hourlyrainin'])

    # cut the chatter. Only change the date when raining stops
    if rain > 0.0 and rain_rate > 0.0 and is_raining == False:
        last_rain_dt = datetime.now(timezone.utc)
        is_raining = True
        write_config('LASTSTATS', 'last_rain', last_rain_dt.isoformat())
        save_config()
    if is_raining and rain_rate == 0.0:
        is_raining = False
        last_rain_dt = datetime.now(timezone.utc)
        write_config('LASTSTATS', 'last_rain', last_rain_dt.isoformat())
        save_config()
    result['lastRain'] = last_rain_dt.strftime('%Y-%m-%dT%H:%M:00.000000%z')

    return result

def application(environ, start_response):
    # the FQDN (i.e. /data?stationtype=AMBWeatherV4.2.9&&tempinf=71.1&humidityin=35)
    # adapted the code to reflect comment from original blog post.
    _LOGGER.debug("Environ is: %s", environ)
    headers = get_headers(environ)
    _LOGGER.debug("Headers: \n\n%s", headers)
    # so remember to set the "path" to "/data?" in the weather station setup.
    result = parse_qs(environ["QUERY_STRING"])
    #_LOGGER.debug("Data: %s", result)
    ws5000_header = [(header, value) for header, value in headers.items() if header.startswith('&STATIONTYPE')]
    _LOGGER.debug("WS-5000 Header: %s", ws5000_header)
    if len(ws5000_header) == 1:
        ws5000_result = {key.lower():value for key, value in parse_qs(':'.join(ws5000_header[0]).lstrip('&').split(' ')[0]).items()}
        _LOGGER.debug("WS-5000 Extra Data: %s", ws5000_result)
        result = result | ws5000_result
    _LOGGER.debug("Full Data: %s", result)
    # send to our other function to deal with the results.
    # result is a dict
    if len(result) == 0:
        start_response("400 Bad Request", [("Content-Type", "text/plain")])
        response_body = "Missing query string"
        _LOGGER.debug("Bad Request: %s", response_body)
        _LOGGER.error("Query string could not be detected in the request. Is the query string character '?' in the path?")
    else:
        handle_results(result)
        # we need to return a response. HTTP code 200 means everything is OK. other HTTP codes include 404 not found and such.
        start_response("200 OK", [("Content-Type", "text/plain")])
        # the response doesn't actually need to contain anything
        response_body = ""
        _LOGGER.debug("Valid Request")
    # return the encoded bytes of the response_body.
    # for python 2 (don't use python 2), the results don't need to be encoded
    return [response_body.encode()]


# this little guy runs a web server if this python file is called directly. if it isn't called directly, it won't run.
# Apache/Python WSGI will run the function 'application()' directly
# in theory, you don't need apache or any web server. just run it right out of python. would need
# to improve error handling to ensure it run without interruption.
if __name__ == "__main__":
    logging.basicConfig(stream = sys.stdout,
                    format = '%(asctime)s %(levelname)8s : %(message)s',
                    level = sys.argv[1])

    # add config file for persistant values and their manipulation
    if config.has_option('LASTSTATS', 'last_rain'):
        last_rain_dt = datetime.fromisoformat(config['LASTSTATS']['last_rain'])
        _LOGGER.warning("Last rain imported from config: %s", last_rain_dt.strftime('%Y-%m-%dT%H:%M:00.000000%z'))
    if config.has_option('LASTSTATS', 'rain_24hr'):
        rain_24hr = float(config['LASTSTATS']['rain_24hr'])
        _LOGGER.warning("24 Hour Rain imported from config: %s", rain_24hr)
    if config.has_option('LASTSTATS', 'rain_yearly'):
        rain_yearly = float(config['LASTSTATS']['rain_yearly'])
        _LOGGER.warning("Yearly Rain imported from config: %s", rain_yearly)
    if config.has_option('SERVER', 'port'):
        server_port = int(config['SERVER']['port'])

	# probably shouldn't run on port 80 but that's what I specified in the ambient weather console
    httpd = make_server("", server_port, application, handler_class=NoLoggingWSGIRequestHandler)
    _LOGGER.info("Serving on http://localhost:" + str(server_port))
    dt = datetime.now()
    # Always show something in the logs once the server starts!
    _LOGGER.error("Server awnet started: " + dt.strftime('%c') + " - Serving on localhost, port:" + str(server_port))

    httpd.serve_forever()
