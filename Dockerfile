FROM python:3.9.12-alpine3.15

ENV LANG C.UTF-8

# Copy data for unsupervised add-on
COPY run.sh /
COPY awnet.py /
RUN chmod a+x /run.sh
RUN pip install requests

# the server runs on this port internally
EXPOSE 8080
VOLUME /config

CMD ["/run.sh"]
#ENTRYPOINT ["python3", "awnet.py", "WARNING"]
