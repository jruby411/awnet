# AWNET

## Description
I do not run a supervised home-assistant instance. I just run the homeassistant docker container. So, in order to facilitate add-ons, I modify the docker containers to work in a separate container if possible.

This is what I have done with the AWNET add-on. But, I am also using this as a learning experience with creating and running customizations with home assistant.

## Background
This is also my first "git" push. I created a new gitlab account and labored trying to get this to the cloud. So I will outline here so I don't forget this in the future.

<details><summary>Click here for my notes</summary>
Once I added an ssh key to my account, I had to add an entry to ~/.ssh/config from information I found here: [LINK](https://stackoverflow.com/a/55149904)

```config
Host gitlab.com
   Hostname altssh.gitlab.com
   User git
   Port 443
   PreferredAuthentications publickey
   IdentityFile ~/.ssh/id_ed25519_gitlab
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

First I went to the subdirectory of files I wanted to upload and typed this:
```bash
cd existing_repo
git init
git add .
git commit -m "Initial commit"
```
Then I could do the following
```bash
git remote add origin ssh://git@gitlab.com/jruby411/awnet
git branch -M main
git push -uf origin main
```
Note: I made an error originally. I used "master" in the place of main. To fix I had to run this command on my local git repository:
```bash
git branch --set-upstream-to=origin/main
```
If you use "main" to start with, you shouldn't have to do this.
</details>

## Build
Download the files in this repository and build the container with:
```bash
docker build -t registry.gitlab.com/jruby411/awnet .
```

## Installation
Install the awnet_local integration from HACS or manually from [awnet_local](https://github.com/tlskinneriv/awnet_local)

If you don't already have a longterm token in Home Assistant, create one and use it for the SUPERVISOR_TOKEN variable below.

Also to note: The ip address 172.17.0.1 is the default gateway for the docker bridge network that I am running the awnet container on. This essentially it the "localhost" to be able to talk to my homeassistant container that is running on port 8123 of the docker HOST network.

## Usage
```config
docker run -d --name awnet \
  --env SUPERVISOR_TOKEN="asdfadsfasdf"
  --env HA_HOST="http://172.17.0.1:8123/api/services/awnet_local/update" \
  --env LOG_LEVEL="WARNING" \
  --env HTTP_SERVER_LOG="NONE" \
  -p 8080:8080 \
  --restart unless-stopped \
  registry.gitlab.com/jruby411/awnet:latest
```

## Authors and acknowledgment
This is a tweak of the integration and add-on combo started by [tlskinneriv](https://github.com/tlskinneriv). The original idea came from [Austin's Nerdy Things](https://austinsnerdythings.com/2021/03/20/handling-data-from-ambient-weather-ws-2902c-to-mqtt/) and was originally adapted for use in Home Assistant by [Dan C Williams](https://github.com/dancwilliams/hassio-addons).

## License
The current license is MIT.
